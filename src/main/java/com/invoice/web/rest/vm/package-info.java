/**
 * View Models used by Spring MVC REST controllers.
 */
package com.invoice.web.rest.vm;
